<?php 

include_once 'application.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if (isset($_POST['submit']))
	{
		//First Name
		if (empty(validate($_POST['firstName'])))
		{
			$_SESSION['firstNameError'] = "First Name field must not empty";
			header('location:create.php');
		}else{
			$firstName = validate($_POST['firstName']);
			$_SESSION['firstName'] = $firstName;
		}

		//Last Name
		if (empty($_POST['lastName']))
		{
			$_SESSION['lastNameError'] = "Last Name field must not empty";
			header('location:create.php');
		}else{
			$lastName = validate($_POST['lastName']);
			$_SESSION['lastName'] = $lastName;
		}

		//Age
		if (empty($_POST['age']))
		{
			$_SESSION['ageError'] = "Age field must not empty";
			header('location:create.php');
		}elseif(!filter_var($_POST['age'], FILTER_VALIDATE_INT))
		{
			$_SESSION['ageError'] = "Invalid age ! Provide valid age";
			header('location:create.php');
		}else{
			$age = validate($_POST['age']);
			$_SESSION['age'] = $age;
		}

		//Gender
		if(empty($_POST['gender']))
		{
			$_SESSION['genderError'] = "Must select gender";
			header('location:create.php');
		}else{
			$gender = validate($_POST['gender']);
			$_SESSION['gender'] = $gender;
		}

		//E-mail
		if(empty($_POST['emailAddress']))
		{
			$_SESSION['emailAddressError'] = "E-mail field must not empty";
			header('location:create.php');
		}elseif(!filter_var($_POST['emailAddress'], FILTER_VALIDATE_EMAIL))
		{
			$_SESSION['emailAddressError'] = "Invalid e-mail address! Provide valid e-mail address";
			$_SESSION['emailAddress'] = $_POST['emailAddress'];
			header('location:create.php');
		}else{
			$emailAddress = validate($_POST['emailAddress']);
			$_SESSION['emailAddress'] = $emailAddress;
		}

		//Hobby
		if(empty($_POST['hobby']))
		{
			$_SESSION['hobbyError'] = "At least select one hobby";
			header('location:create.php');
		}else{
			$hobby = serialize($_POST['hobby']);
			$_SESSION['hobby'] = $hobby;
		}

		//Address
		if (empty(validate($_POST['address'])))
		{
			$_SESSION['addressError'] = "Address field must not empty";
			header('location:create.php');
		}else{
			$address = validate($_POST['address']);
			$_SESSION['address'] = $address;
		}

		//Image
		if(empty($_FILES['image']['name']))
		{
			$_SESSION['imageError'] = "Image must be select";
			header('location:create.php');
		}else{
			$rawImageName = $_FILES['image']['name'];
			$rawImageSize = $_FILES['image']['size'];
			$rawImageExplodeExtentions = explode('.', $rawImageName);
			$rawImageExtention = end($rawImageExplodeExtentions);
			$imageExtention = strtolower($rawImageExtention);

			//Image Format & Size Validation
			$accpectFormat = array('jpg', 'jpeg', 'bmp', 'png', 'gif');
			$maxImageSize = 1048576;
			if (!in_array($imageExtention, $accpectFormat))
			{
				$_SESSION['imageError'] = "Invalid image format select! Image must be in jpg, jpeg, png, bmp, gif format";
				header('location:create.php');

			//Image Size Validation
			}elseif($rawImageSize > $maxImageSize)
			{
				$_SESSION['imageError'] = "Image file size must be less than 1MB";
				header('location:create.php');
			}else{
				$tempImage = $_FILES['image']['tmp_name'];
				$currentDate = Date("d-m-y");
				$uniqueId = substr(md5(time()), 1,10);
				$imageName = str_replace(' ', '-', $firstName.'-'.$lastName).'-'.$currentDate.'-'.$uniqueId.'.'.$imageExtention;
				$uploadDirectory = "uploads/";
				if (!file_exists($uploadDirectory)) {
					mkdir($uploadDirectory,0777,true);
				}
				move_uploaded_file($tempImage, $uploadDirectory.$imageName);
			}
		}
		
		//Store information in session
		if(!array_key_exists('data', $_SESSION))
		{
			$_SESSION['data'] = array();
		}

		if(!empty($firstName) && !empty($lastName) && !empty($age) && !empty($gender) && !empty($emailAddress) && !empty($hobby) && !empty($address) && !empty($imageName))
		{
			$_SESSION['data'][] = array(
									'firstName' => $firstName,
									'lastName' => $lastName,
									'age' => $age,
									'gender' => $gender,
									'emailAddress' => $emailAddress,
									'hobby' => $hobby,
									'address' => $address,
									'image' => $imageName,
								);
			$_SESSION['message'] = "Data successfully stored";

			//Delete temp data
			unset($_SESSION['firstName']);
			unset($_SESSION['lastName']);
			unset($_SESSION['age']);
			unset($_SESSION['gender']);
			unset($_SESSION['emailAddress']);
			unset($_SESSION['hobby']);
			unset($_SESSION['address']);

			header('location:index.php');
		}
	}
}else{
	header('location:index.php');
}