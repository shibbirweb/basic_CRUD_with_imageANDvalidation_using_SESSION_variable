<?php include_once 'header.php'; ?>

<form action="add.php" method="POST" enctype="multipart/form-data">
	<fieldset>
		<legend>Add new record</legend>
		<sup><span class="required">* </span> Fileds are required</sup>
		<table>
			<tr>
				<td><label for="firstName"><span class="required">* </span>First Name</label></td>
				<td>
					<input type="text" name="firstName" id="firstName" placeholder="Type first name"
						<?php
							if (isset($_SESSION['firstName']))
							{
						?>
								value="<?php echo $_SESSION['firstName'];?>"
						<?php
							}
							unset($_SESSION['firstName']);
						?>
					/>
					<span class="error"><?php 
							if(isset($_SESSION['firstNameError']))
							{
								echo $_SESSION['firstNameError'];
								unset($_SESSION['firstNameError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><label for="lastName"><span class="required">* </span>Last Name</label></td>
				<td>
					<input type="text" name="lastName" id="lastName" placeholder="Type last name"
						<?php
							if (isset($_SESSION['lastName']))
							{
						?>
								value="<?php echo $_SESSION['lastName'];?>"
						<?php
							}
							unset($_SESSION['lastName']);
						?>
					/>
					<span class="error"><?php 
							if(isset($_SESSION['lastNameError']))
							{
								echo $_SESSION['lastNameError'];
								unset($_SESSION['lastNameError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><label for="age"><span class="required">* </span>Age</label></td>
				<td>
					<input type="number" name="age" id="age" placeholder="Type age"
						<?php
							if (isset($_SESSION['age']))
							{
						?>
								value="<?php echo $_SESSION['age'];?>"
						<?php
							}
							unset($_SESSION['age']);
						?>
					/>
					<span class="error"><?php 
							if(isset($_SESSION['ageError']))
							{
								echo $_SESSION['ageError'];
								unset($_SESSION['ageError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><label><span class="required">* </span>Gender</label></td>
				<td>
					<input type="radio" name="gender" value="1" id="Male"
						<?php
							if (isset($_SESSION['gender']))
							{
								if($_SESSION['gender'] == 1)
								{
						?>
								checked="checked"
						<?php
								unset($_SESSION['gender']);
								}
							}
						?>
					/>
					<label for="Male">Male</label>
					<input type="radio" name="gender" value="2" id="Female"
						<?php
							if (isset($_SESSION['gender']))
							{
								if($_SESSION['gender'] == 2)
								{
						?>
								checked="checked"
						<?php
								unset($_SESSION['gender']);
								}
							}
						?>
					/>
					<label for="Female">Female</label>
					<span class="error"><?php 
							if(isset($_SESSION['genderError']))
							{
								echo $_SESSION['genderError'];
								unset($_SESSION['genderError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><label for="emailAddress"><span class="required">* </span>E-mail Address</label></td>
				<td>
					<input type="email" name="emailAddress" id="emailAddress" placeholder="Type email address"
						<?php
							if (isset($_SESSION['emailAddress']))
							{
						?>
								value="<?php echo $_SESSION['emailAddress'];?>"
						<?php
							}
							unset($_SESSION['emailAddress']);
						?>
					/>
					<span class="error"><?php 
							if(isset($_SESSION['emailAddressError']))
							{
								echo $_SESSION['emailAddressError'];
								unset($_SESSION['emailAddressError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><label><span class="required">* </span>Hobby</label></td>
				<td>
					<input type="checkbox" name="hobby[]" value="Coding" id="Coding"
						<?php
							if (isset($_SESSION['hobby']))
							{
								if(in_array('Coding', unserialize($_SESSION['hobby'])))
								{
						?>
								checked="checked"
						<?php
								}
							}
						?>
					/>
					<label for="Coding">Coding</label>
					<input type="checkbox" name="hobby[]" value="Browsing" id="Browsing"
						<?php
							if (isset($_SESSION['hobby']))
							{
								if(in_array('Browsing', unserialize($_SESSION['hobby'])))
								{
						?>
								checked="checked"
						<?php
								}
							}
						?>
					/>
					<label for="Browsing">Browsing</label>
					<input type="checkbox" name="hobby[]" value="Hacking" id="Hacking"
						<?php
							if (isset($_SESSION['hobby']))
							{
								if(in_array('Hacking', unserialize($_SESSION['hobby'])))
								{
						?>
								checked="checked"
						<?php
								}
							}
							unset($_SESSION['hobby']);
						?>
					/>
					<label for="Hacking">Hacking</label>
					<span class="error"><?php 
							if(isset($_SESSION['hobbyError']))
							{
								echo $_SESSION['hobbyError'];
								unset($_SESSION['hobbyError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><label for="address"><span class="required">* </span>Address</label></td>
				<td>
					<textarea name="address" id="address" rows="6" placeholder="Type address"><?php
							if (isset($_SESSION['address']))
							{
								echo $_SESSION['address'];
								unset($_SESSION['address']);
							}?></textarea>
					<span class="error"><?php 
							if(isset($_SESSION['addressError']))
							{
								echo $_SESSION['addressError'];
								unset($_SESSION['addressError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td><span class="required">* </span>Image</td>
				<td>
					<input type="file" name="image" accept="image/*">
					<span class="error"><?php 
							if(isset($_SESSION['imageError']))
							{
								echo $_SESSION['imageError'];
								unset($_SESSION['imageError']);
							} 
						?>	
					</span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="warning"><sup><small><small>Image must be in jpg, jpeg, png, bmp, gif format and less than 1MB</small></small></sup></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" value="Submit"> <input type="reset" value="Reset"></td>
			</tr>
		</table>
	</fieldset>
</form>
<p><a href="index.php">Back to list</a></p>
<?php include_once 'footer.php';?>