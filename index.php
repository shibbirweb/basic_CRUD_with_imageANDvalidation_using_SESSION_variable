<?php include_once 'header.php';?>

<?php 
	if (array_key_exists('data', $_SESSION) && !empty($_SESSION['data']))
	{
		$data = $_SESSION['data'];
		//debug($data);
	}

 ?>

<?php 
	//Message
	if (isset($_SESSION['message']))
	{
		?>
		<h3 class="message"><?php echo $_SESSION['message']; ?></h3>
		<?php
		unset($_SESSION['message']);
	}
 ?>
<p><a href="create.php">Click here</a> to create new Record</p>

<table id="index">
	<tr>
		<td>Serial No</td>
		<td>Photo</td>
		<td>Name</td>
		<td>Age</td>
		<td>Gender</td>
		<td>Email Address</td>
		<td>Hobby</td>
		<td>Address</td>
		<td>Action</td>
	</tr>
	<?php
		if (!empty($data)) {
			foreach ($data as $key => $value)
			{
	?>
	<tr>
		<td><?php echo $key+1;?></td>
		<td><img src="uploads/<?php echo $value['image']?>" alt="<?php echo $value['firstName'].' '.$value['lastName'];?>"></td>
		<td><?php echo $value['firstName'].' '.$value['lastName'];?></td>
		<td><?php echo $value['age'];?></td>
		<td>
			<?php 
				if ($value['gender'] == 1) {
					echo "Male";
				}elseif ($value['gender'] == 2) {
					echo "Female";
				}

			 ?>
		</td>
		<td><?php echo $value['emailAddress'];?></td>
		<td>
			<?php 
				if (!empty($value['hobby']))
				{
					$hobbies = unserialize($value['hobby']);
					echo "<ul>";
					foreach ($hobbies as $hobby)
					{
						echo "<li>$hobby</li>";
					}
					echo "</ul>";
				}
			 ?>
		</td>
		<td><?php echo $value['address'];?></td>
		<td>
			<a href="show.php?id=<?php echo $key;?>">Show</a> |
			<a href="edit.php?id=<?php echo $key;?>">Edit</a> |
			<a href="delete.php?id=<?php echo $key;?>" onclick="return confirm('Are you sure to delete this?');">Delete</a>
		</td>
	</tr>
	<?php 
			}
		}else{
	 ?>
	 <tr>
	 	<td colspan="9" align="center">No data avialabel</td>
	 </tr>
	 <?php 
		}
	  ?>
</table>




<?php include_once 'footer.php'; ?>
