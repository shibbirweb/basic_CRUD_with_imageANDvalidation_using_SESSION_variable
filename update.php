<?php 
include_once 'application.php';

if ($_SERVER['REQUEST_METHOD'] == "POST")
{
	if (isset($_POST['update']))
	{
		if (array_key_exists('data', $_SESSION) && !empty($_SESSION['data']))
		{
			$singleData = $_SESSION['data'][$_POST['id']];
		}
		//First Name
		if (empty(validate($_POST['firstName'])))
		{
			$_SESSION['firstNameError'] = "First Name field must not empty";
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$firstName = validate($_POST['firstName']);
		}

		//Last Name
		if (empty($_POST['lastName']))
		{
			$_SESSION['lastNameError'] = "Last Name field must not empty";
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$lastName = validate($_POST['lastName']);
		}

		//Age
		if (empty($_POST['age']))
		{
			$_SESSION['ageError'] = "Age field must not empty";
			header('location:edit.php?id='.$_POST['id']);
		}elseif(!filter_var($_POST['age'], FILTER_VALIDATE_INT))
		{
			$_SESSION['ageError'] = "Invalid age ! Provide valid age";
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$age = validate($_POST['age']);
		}

		//Gender
		if(empty($_POST['gender']))
		{
			$_SESSION['genderError'] = "Must select gender";
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$gender = validate($_POST['gender']);
		}

		//E-mail
		if(empty($_POST['emailAddress']))
		{
			$_SESSION['emailAddressError'] = "E-mail field must not empty";
			header('location:edit.php?id='.$_POST['id']);
		}elseif(!filter_var($_POST['emailAddress'], FILTER_VALIDATE_EMAIL))
		{
			$_SESSION['emailAddressError'] = "Invalid e-mail address! Provide valid e-mail address";
			$_SESSION['emailAddress'] = $_POST['emailAddress'];
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$emailAddress = validate($_POST['emailAddress']);
		}

		//Hobby
		if(empty($_POST['hobby']))
		{
			$_SESSION['hobbyError'] = "At least select one hobby";
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$hobby = serialize($_POST['hobby']);
		}

		//Address
		if (empty(validate($_POST['address'])))
		{
			$_SESSION['addressError'] = "Address field must not empty";
			header('location:edit.php?id='.$_POST['id']);
		}else{
			$address = validate($_POST['address']);
		}

		//Image
		if(empty($_FILES['image']['name']))
		{
			$imageName = $_SESSION['data'][$_POST['id']]['image'];
		}else{
			$rawImageName = $_FILES['image']['name'];
			$rawImageSize = $_FILES['image']['size'];
			$rawImageExplodeExtentions = explode('.', $rawImageName);
			$rawImageExtention = end($rawImageExplodeExtentions);
			$imageExtention = strtolower($rawImageExtention);

			//Image Format & Size Validation
			$accpectFormat = array('jpg', 'jpeg', 'bmp', 'png', 'gif');
			$maxImageSize = 1048576;
			if (!in_array($imageExtention, $accpectFormat))
			{
				$_SESSION['imageError'] = "Invalid image format select! Image must be in jpg, jpeg, png, bmp, gif format";
				header('location:edit.php?id='.$_POST['id']);

			//Image Size Validation
			}elseif($rawImageSize > $maxImageSize)
			{
				$_SESSION['imageError'] = "Image file size must be less than 1MB";
				header('location:edit.php?id='.$_POST['id']);
			}else{
				$tempImage = $_FILES['image']['tmp_name'];
				$currentDate = Date("d-m-y");
				$uniqueId = substr(md5(time()), 1,10);
				$imageName = str_replace(' ', '-', $firstName.'-'.$lastName).'-'.$currentDate.'-'.$uniqueId.'.'.$imageExtention;
				$uploadDirectory = "uploads/";
				if (!file_exists($uploadDirectory)) {
					mkdir($uploadDirectory,0777,true);
				}
				if (file_exists($uploadDirectory.$singleData['image'])) {
					unlink($uploadDirectory.$singleData['image']);
				}
				move_uploaded_file($tempImage, $uploadDirectory.$imageName);
			}
		}

		//Upadate information in session
		
		if(!empty($firstName) && !empty($lastName) && !empty($age) && !empty($gender) && !empty($emailAddress) && !empty($hobby) && !empty($address) && !empty($imageName))
		{
			$_SESSION['data'][$_POST['id']] = array(
									'firstName' => $firstName,
									'lastName' => $lastName,
									'age' => $age,
									'gender' => $gender,
									'emailAddress' => $emailAddress,
									'hobby' => $hobby,
									'address' => $address,
									'image' => $imageName,
								);
			$_SESSION['message'] = "Data successfully updated";
			header('location:index.php');
		}
	}
}else{
	header('location:index.php');
}