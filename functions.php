<?php 
	function debug($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}


	function validate($data)
	{
		$data = htmlspecialchars($data);
		$data = trim($data);
		$data = stripcslashes($data);
		return $data;
	}
