<?php
include_once 'application.php';
if (isset($_GET['id']))
{
	if (array_key_exists('data', $_SESSION) && !empty($_SESSION['data']))
	{
		if (file_exists("uploads/".$_SESSION['data'][$_GET['id']]['image']))
		{
			unlink("uploads/".$_SESSION['data'][$_GET['id']]['image']);
		}
		unset($_SESSION['data'][$_GET['id']]);
		$_SESSION['message'] = "Data successfully deleted";
		header('Location:index.php');
	}
}