<?php include_once 'header.php'; ?>

<?php if (isset($_GET['id']))
{
	if (array_key_exists('data', $_SESSION) & !empty($_SESSION['data'])) {
		$singleData = $_SESSION['data'][$_GET['id']];
	}
	
}else{
	header('location:index.php');
}
?>
<div class="contentShow">
	<h3 class="heading"><?php echo $singleData['firstName'].' '.$singleData['lastName']; ?>'s Information</h3>
	<div class="contentImage">
		<img src="uploads/<?php echo $singleData['image'];?>" alt="<?php echo $singleData['firstName'].' '.$singleData['lastName'];?>"/>
	</div>
	<table id="show">
		<tr>
			<td>Name</td>
			<td><?php echo $singleData['firstName'].' '.$singleData['lastName'];?></td>
		</tr>
		<tr>
			<td>Age</td>
			<td><?php echo $singleData['age'];?></td>
		</tr>
		<tr>
			<td>Gender</td>
			<td>
				<?php 
					if ($singleData['gender'] == 1) {
					echo "Male";
					}elseif ($singleData['gender'] == 2) {
						echo "Female";
					}
				?>	
			</td>
		</tr>
		<tr>
			<td>E-mail Address</td>
			<td><?php echo $singleData['emailAddress'];?></td>
		</tr>
		<tr>
			<td>Hobby</td>
			<td>
				<?php 
				if (!empty($singleData['hobby']))
				{
					$hobbies = unserialize($singleData['hobby']);
					echo "<ul>";
					foreach ($hobbies as $hobby)
					{
						echo "<li>$hobby</li>";
					}
					echo "</ul>";
				}
			 ?>
			</td>
		</tr>
		<tr>
			<td>Address</td>
			<td><?php echo $singleData['address']; ?></td>
		</tr>
		<tr>
			<td>Action</td>
			<td>
				<a href="edit.php?id=<?php echo $_GET['id'];?>">Edit</a> |
				<a href="delete.php?id=<?php echo $_GET['id'];?>" onclick="return confirm('Are you sure to delete this?');">Delete</a>
			</td>
		</tr>
	</table>
</div>
<p><a href="index.php">Back to list</a></p>

<?php include_once 'footer.php'; ?>